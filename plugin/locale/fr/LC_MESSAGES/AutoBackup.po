msgid ""
msgstr ""
"Project-Id-Version: enigma2-plugin-extensions-autobackup\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-07 11:02+0100\n"
"PO-Revision-Date: 2021-11-07 11:02+0100\n"
"Last-Translator: Pr2\n"
"Language-Team: french\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.0\n"

#: ../plugin.py:137
msgid "Auto backup"
msgstr "Sauvegarde automatique"

#: ../ui.py:104 ../ui.py:152
msgid "AutoBackup Configuration"
msgstr "Configuration de AutoBackup"

#: ../plugin.py:141
msgid "Automatic settings backup"
msgstr "Sauvegarde automatique des réglages"

#: ../ui.py:122
msgid "Automatic start time"
msgstr "Heure de lancement automatique"

#: ../ui.py:203
msgid "Backup EPG cache"
msgstr "Sauvegarder le cache EPG"

#: ../ui.py:120
msgid "Backup location"
msgstr "Emplacement de la sauvegarde"

#: ../ui.py:24 ../ui.py:43
msgid "CF"
msgstr "CF"

#: ../ui.py:128 ../ui.py:373
msgid "Cancel"
msgstr "Annuler"

#: ../ui.py:321
msgid ""
"Choose a backup.\n"
"This will delete autoinstall list.\n"
"Do you really want to continue?"
msgstr ""
"Choisir une sauvegarde.\n"
"Cela effacera la liste autoinstall.\n"
"Voulez-vous vraiment continuer?"

#: ../ui.py:292
msgid ""
"Choose a backup.\n"
"This will reinstall all plugins from your backup.\n"
"Do you really want to reinstall?"
msgstr ""
"Choisir une sauvegarde.\n"
"Ceci va réinstaller tous les plugins/extensions de votre sauvegarde sur.\n"
"Voulez-vous vraiment les réinstaller?"

#: ../ui.py:253
msgid ""
"Choose settings backup which should be restored.\n"
"Do you really want to restore these settings and restart?"
msgstr ""
"Choisir les paramètres de la sauvegarde que vous voulez restaurer.\n"
"Voulez-vous vraiment restaurer ces réglages et redémarrer?"

#: ../ui.py:123
msgid "Create Autoinstall"
msgstr "Créer une installation automatique"

#: ../ui.py:121
msgid "Daily automatic backup"
msgstr "Sauvegarde automatique quotidienne"

#: ../ui.py:412
msgid "Deselect"
msgstr "Désélectionner"

#: ../ui.py:260
msgid "Do you want to restore your settings?"
msgstr "Voulez-vous restaurer les paramètres?"

#: ../ui.py:345
msgid "Done"
msgstr "Effectué"

#: ../ui.py:124
msgid "EPG cache backup"
msgstr "Sauvegarde du cache EPG"

#: ../ui.py:343
msgid "Failed"
msgstr "Echoué"

#: ../ui.py:22 ../ui.py:45
msgid "Harddisk"
msgstr "Disque dur"

#: ../ui.py:178
msgid "Last backup date"
msgstr "Date de la dernière sauvegarde"

#: ../ui.py:130
msgid "Manual"
msgstr "Manuel"

#: ../ui.py:289 ../ui.py:318
msgid "No autoinstall list found"
msgstr "Pas de fichier d’auto-installation trouvé"

#: ../ui.py:180 ../ui.py:183
msgid "No backup present"
msgstr "Aucune sauvegarde présente"

#: ../ui.py:250
msgid "No settings backups found"
msgstr "Pas de sauvegarde de paramètres trouvé"

#: ../ui.py:171
msgid "No suitable media found, insert USB stick, flash card or harddisk."
msgstr "Aucun média approprié trouvé, insérez une clé USB, carte flash ou disque dur."

#: ../ui.py:117
msgid "Nowhere"
msgstr "Nulle part"

#: ../ui.py:129
msgid "OK"
msgstr "Ok"

#: ../ui.py:426
msgid "Really close without saving settings?"
msgstr "Voulez-vous vraiment quitter sans sauver les paramètres?"

#: ../ui.py:205
msgid "Remove autoinstall list"
msgstr "Enlever la liste d’auto-installation"

#: ../ui.py:131 ../ui.py:206
msgid "Restore"
msgstr "Restaurer"

#: ../ui.py:202
msgid "Run a backup now"
msgstr "Lancer une sauvegarde maintenant"

#: ../ui.py:204
msgid "Run autoinstall"
msgstr "Exécuter l’auto-installation"

#: ../ui.py:231 ../ui.py:270 ../ui.py:299
msgid "Running..."
msgstr "En cours d’exécution…"

#: ../ui.py:25
msgid "SD"
msgstr "SD"

#: ../ui.py:374
msgid "Save"
msgstr "Sauvegarder"

#: ../ui.py:125
msgid "Save previous backup"
msgstr "Enregistrer la sauvegarde précédente"

#: ../ui.py:414
msgid "Select"
msgstr "Sélectionner"

#: ../ui.py:201
msgid "Select files to backup"
msgstr "Sélectionner les fichiers à sauvegarder"

#: ../ui.py:405
msgid "Select files/folders to backup"
msgstr "Sélectionner les fichiers/dossiers à sauvegarder"

#: ../ui.py:23
msgid "USB"
msgstr "USB"

#: ../ui.py:245 ../ui.py:284 ../ui.py:313
msgid "from: "
msgstr "De: "

#: ../ui.py:40
msgid "root"
msgstr "racine"
